TEMPLATE = {
    'andn dest,src1,src2':[
        'xori dep1,src2,-1',
        'and dest,dep1,src1'
    ],

    'orn dest,src1,src2':[
        'xori dep1,src2,-1',
        'or dest,dep1,src1'
    ],

    'xnor dest,src1,src2':[
        'xori dep1,src2,-1',
        'xor dest,dep1,src1'
    ],

    'cmov dest,src1,src2,src3':[
        'and dep1,src1,src2',
        'xori dep2,src2,-1',
        'and dep3,dep2,src3',
        'or dest,dep3,dep1'
    ],

    'pack dest,src1,src2':[
        'andi dep1,src1,0x0000FFFF',
        'slli dep2,src2,16',
        'or dest,dep1,dep2'
    ],

    'rol dest,src1,src2':[
        'andi dep1,src2,31',
        'sll dep2,src1,dep1',
        'sub dep3,x0,src2',
        'andi dep3,dep3,31',
        'srl dep5,src1,dep3',
        'or dest,dep5,dep2'
    ],

    'ror dest,src1,src2':[
        'andi dep1,src2,31',
        'srl dep2,src1,dep1',
        'sub dep3,x0,src2',
        'andi dep3,dep3,31',
        'sll dep5,src1,dep3',
        'or dest,dep5,dep2'
    ],

    'sro dest,src1,src2':[
        'andi dep1,src2,31',
        'xori dep2,src1,-1',
        'srl dep3,dep2,dep1',
        'xori dest,dep3,-1'
    ],

    'slo dest,src1,src2':[
        'andi dep1,src2,31',
        'xori dep2,src1,-1',
        'sll dep3,dep2,dep1',
        'xori dest,dep3,-1'
    ],

    'sbset dest,src1,src2':[
        'andi dep1,src2,31',
        'addi dep2,x0,1',
        'sll dep3,dep2,dep1',
        'or dest,src1,dep3'
    ],

    'sbext dest,src1,src2':[
        'andi dep1,src2,31',
        'srl dep2,src1,dep1',
        'andi dest,dep2,1'
    ],

    'sbinv dest,src1,src2':[
        'andi dep1,src2,31',
        'addi dep2,x0,1',
        'sll dep3,dep2,dep1',
        'xor dest,dep3,src1'
    ],

    'sbclr dest,src1,src2':[
        'andi dep1,src2,31',
        'addi dep2,x0,1',
        'sll dep3,dep2,dep1',
        'xori dep4,dep3,-1',
        'and dest,src1,dep4'
    ],

    'load32 dest,immd':[
        'lui rd,imm1',
        'addi rd,rd,imm2'
    ],

    'and32 dest,src1,immd':[
        'lui dest,imm1',
        'addi dest,dest,imm2',
        'and dest,dest,src1'
    ],

    'or32 dest,src1,immd':[
        'lui dest,imm1',
        'addi dest,dest,imm2',
        'or dest,dest,src1'
    ],

    'xor32 dest,src1,immd':[
        'lui dest,imm1',
        'addi dest,dest,imm2',
        'xor dest,dest,src1'
    ],

    'add32 dest,src1,immd':[
        'lui dest,imm1',
        'addi dest,dest,imm2',
        'add dest,dest,src1'
    ],

    'sub32 dest,src1,immd':[
        'lui dest,imm1',
        'addi dest,dest,imm2',
        'sub dest,dest,src1'
    ],

    'nand dest,src1,src2':[
        'and dest,src1,src2',
        'xori dest,dest,-1'
    ],

    'nor dest,src1,src2':[
        'or dest,src1,src2',
        'xori dest,dest,-1'
    ],

    'xnor dest,src1,src2':[
        'xor dest,src1,src2',
        'xori dest,dest,-1'
    ],

    'addnot dest,src1,src2':[
        'add dest,src1,src2',
        'xori dest,dest,-1'
    ],

    'subnot dest,src1,src2':[
        'sub dest,src1,src2',
        'xori dest,dest,-1'
    ]
}
