import spikepost.parsers as parsers

def test_opcode_mask():
    assert parsers.OPCODE_MASK & 0x00000037 == 0b0110111

def test_lui():
    instr = parsers.parseInstruction(r'core   0: 0x00000000800010d0 (0x00001fb7) lui     t6, 0x1')
    assert instr.instr_name == 'lui'
    assert instr.imm == 0b01
    assert instr.rd == 31

def test_auipc():
    instr = parsers.parseInstruction(r'core   0: 0x0000000080001108 (0x00001717) auipc   a4, 0x1')
    assert instr.instr_name == 'auipc'
    assert instr.imm == 0b01
    assert instr.rd == 14

def test_jal():
    instr = parsers.parseInstruction(r'core   0: 0x0000000080001f98 (0x914ff0ef) jal     pc - 0xeec')
    assert instr.instr_name == 'jal'
    assert 2*instr.imm == -0xeec
    assert instr.rd == 1

def test_jalr():
    instr = parsers.parseInstruction(r'core   0: 0x0000000080001bf8 (0xf40080e7) jalr    ra, ra, -192')
    assert instr.instr_name == 'jalr'
    assert instr.imm == -192
    assert instr.rd == 1
    assert instr.rs1 == 1

def test_beq():
    instr = parsers.parseInstruction(r'core   0: 0x0000000080001154 (0x02d78663) beq     a5, a3, pc + 44')
    assert instr.instr_name == 'beq'
    assert instr.imm == 22
    assert instr.rs1 == 15 
    assert instr.rs2 == 13 

def test_bne():
    instr = parsers.parseInstruction(r'core   0: 0x0000000080001fe8 (0xfd089ce3) bne     a7, a6, pc - 40')
    assert instr.instr_name == 'bne'
    assert instr.imm == -20
    assert instr.rs1 == 17
    assert instr.rs2 == 16

# TODO: Test other Branch Ops

def test_lbu():
    instr = parsers.parseInstruction(r'core   0: 0x0000000080001d08 (0xfff54783) lbu     a5, -1(a0)')
    assert instr.instr_name == 'lbu'
    assert instr.rd == 15
    assert instr.rs1 == 10
    assert instr.imm == -1

# TODO Test Other Load Ops

def test_sb():
    instr = parsers.parseInstruction(r'core   0: 0x0000000080001f28 (0x5ae68ca3) sb      a4, 1465(a3)')
    assert instr.instr_name == 'sb'
    assert instr.rs1 == 13
    assert instr.rs2 == 14
    assert instr.imm == 1465

# TODO: Test other Store ops

def test_addi():
    instr = parsers.parseInstruction(r'core   0: 0x0000000080001f1c (0x03010593) addi    a1, sp, 48')
    assert instr.instr_name == 'addi'
    assert instr.imm == 48
    assert instr.rd == 11
    assert instr.rs1 == 2

def test_srli():
    instr = parsers.parseInstruction(r'core   0: 0x0000000080001f1c (0x00f35293) srli    t0, t1, 15')
    assert instr.instr_name == 'srli'
    assert instr.shamt == 15
    assert instr.rd == 5
    assert instr.rs1 == 6

def test_srai():
    instr = parsers.parseInstruction(r'core   0: 0x0000000080001f1c (0x40f35293) srai    t0, t1, 15')
    assert instr.instr_name == 'srai'
    assert instr.shamt == 15
    assert instr.rd == 5
    assert instr.rs1 == 6

def test_slli():
    instr = parsers.parseInstruction(r'core   0: 0x00000000800010b8 (0x00261613) slli    a2, a2, 2')
    assert instr.instr_name == 'slli'
    assert instr.shamt == 2
    assert instr.rd == 12
    assert instr.rs1 == 12
