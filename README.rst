RITA - RISC-V Trace Analyzer
=========

We are only supporting Python3 for this code lib. Please use the library with Python3 only.

A tool to post-process trace files generated from the riscv-isa-simulator(spike).

Introduction
------------
For those looking to perform common post-processing options on the instruction trace files generated from spike, this command can be handy. It packs common features like histograms, loop depths and others.

The basic usage is as:

.. code-block:: console

        usage: python -m rita.main [-h] [--trace-file FILE] [--addr-min ADDR_MIN]
                         [--addr-max ADDR_MAX] [--hist-size HSIZE] [--version]
                         [--list-stats] [--stat STAT]

Installation
____________
The easiest way to install this package is:

.. code-block:: console

    git clone https://gitlab.com/shaktiproject/tools/rita && cd rita
    export PYTHONPATH=$PYTHONPATH:$(pwd)
    python3 -m rita.main

Getting Started
---------------
Write a simple Hello World code in C, and after you compile your code using the riscv-gcc compiler, you have to exectue the program as follows -

.. code-block:: console

        riscv64-unknown-elf-gcc main.c -o main
        spike -l pk main 2>log

The last redirection writes the instruction trace to the file called *log*.

After this setup, you can check the histogram like so:

.. code-block:: console

        python3 -m rita.main --trace-file log --stat hist_instr
