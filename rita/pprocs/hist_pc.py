import rita.parsers
from collections import Counter
import sys

'''Program counter histogram post processing module'''

help_message = '{:<20s} {:<10s}'.format('hist_pc', 'Histogram of program counter values')

def compute(input_file, args):
    '''Compute the histogram'''
    pc_cnt = Counter()
    total_insts = 0
    output_size = None if args.output_size is None else args.output_size
    addr_min = None if args.addr_min is None else args.addr_min
    addr_max = None if args.addr_max is None else args.addr_max
   
    with open(input_file) as fp:
        for line in fp: 
            instr = rita.parsers.parseInstruction(line, args.mode)
            if instr is None:
                print("Skipping {0}".format(line), file=sys.stderr)
                continue

            addr = instr.instr_addr
            if addr_min is not None and addr < addr_min:
                continue   
            if addr_max is not None and addr > addr_max:
                break

            if instr.instr_name is not None:
                total_insts += 1
                pc_cnt[addr]+=1

    print("  Histogram of program counter\n")
    print("  Total number of instructions: {0:d}\n".format(total_insts))
    print("  {0:<10s} {1:<10s} {2:<10s}".format("PC Value", "Count", "Percentage"))
    print("  -------------------------------------")
    
    for inst in pc_cnt.most_common(output_size):
        print("  0x{0:<8x} {1:<10} {2:<10.2f}".format(inst[0], inst[1], 100*inst[1]/total_insts))    
