import rita.parsers
from collections import Counter
import sys

'''Histogram post-processing module'''

help_message = '{:<20s} {:<10s}'.format('hist_instr', 'Histogram of instructions')

def compute(input_file, args):
    '''Compute the histogram'''
    cnt = Counter()
    total_insts = 0
    output_size = None if args.output_size is None else args.output_size
    addr_min = None if args.addr_min is None else args.addr_min
    addr_max = None if args.addr_max is None else args.addr_max
    mode = args.mode
  
    with open(input_file) as fp:
        for line in fp:
            instr = rita.parsers.parseInstruction(line, mode)

            if instr is None:
                print("Skipping {0}".format(line), file=sys.stderr)
                continue

            addr = instr.instr_addr
            if addr_min is not None and addr < addr_min:
                continue
            if addr_max is not None and addr > addr_max:
                break


            if instr.instr_name is not None:
                cnt[instr.instr_name] += 1
                total_insts += 1

    print("Histogram of Instructions\n")
    print("  Total number of instructions: {0:d}\n".format(total_insts))
    print("  {0:<10s} {1:<10s} {2:<10s}".format("Name", "Frequency", "Percentage"))
    print("  -------------------------------------")

    for instr in cnt.most_common(output_size):
        print("  {0:<10s} {1:<10} {2:<10.2f}".format(instr[0], instr[1], 100*instr[1]/total_insts))
