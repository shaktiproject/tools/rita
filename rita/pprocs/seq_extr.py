import rita.parsers
from collections import Counter
import re
import sys
import os
from pathlib import Path
file1 = []
file = []
help_message = '{:<20s} {:<10s}'.format('seq_extr', 'Extraction of Sequences')
opt_list2 = []
opt_list3 = []
opt_list4 = []
pc_replace21 = []
pc_replace22 = []
pc_replace23 = []
pc_replace31 = []
pc_replace32 = []
pc_replace33 = []
pc_replace34 = []
pc_replace41 = []
pc_replace42 = []
pc_replace43 = []
pc_replace44 = []
pc_replace45 = []


def rep_two(file,opt_list2):
    i = 0
    line_no1 = 0
    line_no2 = 0
    while(i<len(opt_list2)):
        line_no1 = opt_list2[i][0]
        line_no2 = opt_list2[i][1]
        file[line_no2] = opt_list2[i][2]
        del file[line_no1]
        j = i + 1
        while(j<len(opt_list2)):
            opt_list2[j][0] = opt_list2[j][0]-1
            opt_list2[j][1] = opt_list2[j][1]-1
            j = j + 1
        i = i + 1
def rep_three(file,opt_list3):
    i = 0
    line_no1 = 0
    line_no2 = 0
    line_no3 = 0
    while(i<len(opt_list3)):
        line_no1 = opt_list3[i][0]
        line_no2 = opt_list3[i][1]
        line_no3 = opt_list3[i][2]
        file[line_no3] = opt_list3[i][3]
        del file[line_no1]
        del file[line_no2-1]
        j = i + 1
        while(j<len(opt_list3)):
            opt_list3[j][0] = opt_list3[j][0]-2
            opt_list3[j][1] = opt_list3[j][1]-2
            opt_list3[j][2] = opt_list3[j][2]-2
            j = j + 1
        i = i + 1
def rep_four(file,opt_list4):
    
    i = 0
    line_no1 = 0
    line_no2 = 0
    line_no3 = 0
    line_no4 = 0
    while(i<len(opt_list4)):
        
        line_no1 = opt_list4[i][0]
        line_no2 = opt_list4[i][1]
        line_no3 = opt_list4[i][2]
        line_no4 = opt_list4[i][3]
        file[line_no4] = opt_list4[i][4]
        del file[line_no1]
        del file[line_no2-1]
        del file[line_no3-2]
        j = i + 1
        while(j<len(opt_list4)):
            opt_list4[j][0] = opt_list4[j][0]-3
            opt_list4[j][1] = opt_list4[j][1]-3
            opt_list4[j][2] = opt_list4[j][2]-3
            opt_list4[j][3] = opt_list4[j][3]-3
            j = j + 1
        i = i + 1
def line_no_correct(file):
    i = 0
    while(i<len(file)):
        file[i][8] = i + 1
        i = i + 1


def two_instruct(file,check_condition,template,name,dest,src1,src2):
    if_list1 = []
    if_list2 = []
    if_list3 = []
    if_list4 = []
    p = 0
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# splitting the dependencies into 4 different lists for easy chaecking in 'statement - 2'
    while(p<len(check_condition)):
        temp1 = check_condition[p].split(';')
        temp11 = str(temp1[0]).split(',')
        temp12 = str(temp1[1]).split(',')
        if_list1.append(temp11[0])
        if_list2.append(temp11[1])
        if_list3.append(temp12[0])
        if_list4.append(temp12[1])
        p = p + 1
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    print(name)
    immchi = 0
    immchj = 0
    immcheck = []
    imm_check_sum = []
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Checking the if the instruction has immediate, and if the immediate is generic or specific
    while(immchi<len(template)-1):
        tx = []
        if(template[immchi][0][len(template[immchi][0])-1]=='i'):
            imre = re.match('imm.*',template[immchi][len(template[immchi])-1])
            if(imre): # --> Generic immediate
                donothing = 0
                tx.append(immchi)
                tx.append(len(template[immchi])-1)
                imm_check_sum.append(tx)
            else: # --> Specific immediate
                tx.append(immchi)
                tx.append(len(template[immchi])-1)
                try:
                    int(template[tx[0]][tx[1]],16)
                except:
                    print('invalid immediate value in template',name)
                    return
                immcheck.append(tx)
        immchi = immchi + 1
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    

    i = 0
    cnt1 = 0
    cnt2 = 0
    m1 = [] # Dummy variable, which gets equated to m[0] after first iteration
    m2 = [] # Dummy variable, which gets equated to m[1] after first iteration
    cnt = []
    cnt.append(0)
    cnt.append(0)
    m = [None,None]
    went_inside = False
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# parsing the LOG FILE in memory, line by line 
    while(i<len(file)):
        went_inside = False
        if(file[i][0] == template[0][0]):
            went_inside = True
            if cnt[0] == 0:
                j = 0
                while(j<len(m1)):
                    if(file[i][1] == m1[j][1]):
                        del m1[j]
                    j = j + 1
                loop = 0
                append_ok = True
                while(loop<len(immcheck)):
                    if(immcheck[loop][0] == 0):
                        if(template[immcheck[loop][0]][immcheck[loop][1]]==file[i][5]): # --> checking if instruction has immediate and if that immediate has required value
                            append_ok = True
                        else:
                            append_ok = False
                    loop = loop + 1
                if(append_ok == True):            
                    m1.append(file[i])
                    m[0] = m1
                    cnt[0] = cnt[0] + 1
                else:
                    ok = 'ok'
            else:
                j = 0
                while(j<len(m[0])):
                    if(file[i][1] == m[0][j][1]): # --> overwriting previously stored line if it has same destination
                        del m[0][j]
                    j = j + 1
                loop = 0
                append_ok = True
                while(loop<len(immcheck)):
                    if(immcheck[loop][0] == 0):
                        if(template[immcheck[loop][0]][immcheck[loop][1]]==file[i][5]): # --> checking if instruction has immediate and if that immediate has required value
                            append_ok = True
                        else:
                            append_ok = False
                    loop = loop + 1
                if(append_ok == True):            
                    m[0].append(file[i])
                else:
                    ok = 'ok'
        if(file[i][0] == template[1][0]):
            went_inside = True
            if cnt[1] == 0:
                j = 0
                while(j<len(m2)):
                    if(file[i][1] == m2[j][1]): # --> overwriting previously stored line if it has same destination
                        del m2[j]
                    j = j + 1
                loop = 0
                append_ok = True
                while(loop<len(immcheck)):
                    if(immcheck[loop][0] == 1):
                        if(template[immcheck[loop][0]][immcheck[loop][1]]==file[i][5]): # --> checking if instruction has immediate and if that immediate has required value
                            append_ok = True
                        else:
                            append_ok = False
                    loop = loop + 1
                if(append_ok == True):            
                    m2.append(file[i])
                    m[1] = m2
                    cnt[1] = cnt[1] + 1
                else:
                    ok = 'ok'
            else:
                j = 0
                while(j<len(m[1])):
                    if(file[i][1] == m[1][j][1]):
                        del m[1][j]
                    j = j + 1
                loop = 0
                append_ok = True
                while(loop<len(immcheck)):
                    if(immcheck[loop][0] == 1):
                        if(template[immcheck[loop][0]][immcheck[loop][1]]==file[i][5]): # --> checking if instruction has immediate and if that immediate has required value
                            append_ok = True
                        else:
                            append_ok = False
                    loop = loop + 1
                if(append_ok == True):            
                    m[1].append(file[i])
                else:
                    ok = 'ok'
                

        if(went_inside == False): # --> Checking if the line contains an instruction that is not part of the template
            k = 0
            while(k<len(m)):
                j = 0
                while(cnt[k] != 0 and j<len(m[k])):
                    if(src1[0] == k and file[i][1] == m[src1[0]][j][src1[1]]): # --> remove if scr1 is overwritten
                        del m[src1[0]][j]
                        j = j - 1
                    elif(src2[0] == k and file[i][1] == m[src2[0]][j][src2[1]]): # -->remove if src2 is overwritten
                        del m[src2[0]][j]
                        j = j - 1
                    elif(file[i][1] == m[k][j][1]): # --> remove is destination register is altered
                        del m[k][j]
                        j = j - 1
                    j = j + 1
                k = k + 1

        j=0
        k=0
        l=0

        flag1 = False
        flagcnt = False
        flagint = 0
        while(flagint<len(cnt)):
            if cnt[flagint] == 0:
                flagcnt = True
            flagint = flagint + 1
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # Checking dependencies
        while(flagcnt == False and j<len(m[1])):
            k = 0
            while(len(m[0])>0 and k<len(m[0]) and m[0][k][8]<m[1][j][8]):
                zz = 0
                Flag = False
                tt = [] # --> temporary variable created to check dependency satisfiablity
                tt.append(m[0][k])
                tt.append(m[1][j])
                while(zz<len(if_list1)):
                    if(tt[int(if_list1[zz])][int(if_list2[zz])] == tt[int(if_list3[zz])][int(if_list4[zz])]): # --> Statement 2
                        useless = 1
                    else:
                        Flag = True
                        break
                    zz = zz + 1
                if(Flag == False):
                    tt = []
                    tt.append(m[0][k])
                    tt.append(m[1][j])  

                    temp_sum = 0
                    sum1 = 0

                    if(len(imm_check_sum)==2):
                        sum1 = sum1 + int(tt[imm_check_sum[temp_sum][0]][5])<<12
                        temp_sum = temp_sum + 1
                        sum1 = sum1 + int(tt[imm_check_sum[temp_sum][0]][5])
                    if((sum1<int('FFFFFFFF',16) and src2[0]==999) or (src2[0]!=999)):
                        replace = []
                        line_no1 = m[0][k][8]-1
                        line_no2 = m[1][j][8]-1
                        replace.append(name)
                        replace.append(tt[dest[0]][dest[1]])
                        replace.append(tt[src1[0]][src1[1]])
                        if(src2[0]==999):
                            replace.append(str(sum1))
                        else:
                            replace.append(tt[src2[0]][src2[1]])
                        replace.append('None')
                        replace.append('None')
                        replace.append('None')
                        replace.append('None')
                        replace.append(line_no1+1)
                        replace.append(file[line_no2][9])
                        

                        r = replace[0]+' '+replace[1]+','+replace[2]+','+replace[3]
                        rep = []
                        hexx = tt[0][9]
                        conv_str = str(hex(int(hexx)))
                        conv_str = conv_str[2:]
                        conv_str = conv_str[8:16]
                        pc_replace21.append(conv_str)
                        hexx = tt[1][9]
                        conv_str = str(hex(int(hexx)))
                        conv_str = conv_str[2:]
                        conv_str = conv_str[8:16]
                        pc_replace22.append(conv_str)
                        pc_replace23.append(r)

                        add = []
                        add.append(line_no1)
                        add.append(line_no2)
                        add.append(replace)
                        print(add)
                        opt_list2.append(add)  
                        
                        rlno = line_no1
                        
                        del m[0][k]
                        del m[1][j]
                        flag1 = True
                        break
                k = k + 1
            if flag1 == True:
                break
            j = j + 1
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        i= i + 1
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


def three_instruct(file,check_condition,template,name,dest,src1,src2):
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Same Logic as 2 instruct except for Block 1 and Block 2    
    set_ignoreT = None
    set_ignoreF = None
    ii = 0
    jj = 0
    breakflag = False
    while(ii<len(template)):                            # Block 1 - 
        jj = ii + 1                                     # 
        while(jj<len(template)):                        #
            if(template[ii][0] == template[jj][0]):     #
                breakflag = True                        #
                set_ignoreT = jj - 1                    #
                set_ignoreF = jj + 1                    #   Checking if any instructions repeat within template itself
                break                                   #
            jj = jj + 1                                 #
        if(breakflag==True):                            #
            break                                       #
        ii = ii + 1                                     #
    
    immchi = 0
    immchj = 0
    immcheck = []
    imm_check_sum = []
  
    while(immchi<len(template)-1):
        tx = []
        if(template[immchi][0][len(template[immchi][0])-1]=='i'):
            imre = re.match('imm.*',template[immchi][len(template[immchi])-1])
            if(imre):
                donothing = 0
                tx.append(immchi)
                tx.append(len(template[immchi])-1)
                imm_check_sum.append(tx)
            else:
                tx.append(immchi)
                tx.append(len(template[immchi])-1)
                try:
                    int(template[tx[0]][tx[1]],16)
                except:
                    print('invalid immediate value in template',name)
                    return
                immcheck.append(tx)
        immchi = immchi + 1

    if_list1 = []
    if_list2 = []
    if_list3 = []
    if_list4 = []
    p = 0
    while(p<len(check_condition)):
        temp1 = check_condition[p].split(';')
        temp11 = str(temp1[0]).split(',')
        temp12 = str(temp1[1]).split(',')
        if_list1.append(temp11[0])
        if_list2.append(temp11[1])
        if_list3.append(temp12[0])
        if_list4.append(temp12[1])
        p = p + 1

    print(name)

    i = 0
    cnt1 = 0
    cnt2 = 0
    m1 = []
    m2 = []
    m3 = []
    cnt = []
    cnt.append(0)
    cnt.append(0)
    cnt.append(0)
    m = [None,None,None]
    went_inside = False
    ignore_flag = False
    while(i<len(file)):
        went_inside = False
        if(file[i][0] == template[0][0]):
            went_inside = True
            if set_ignoreT != None and set_ignoreT == 0:
                ignore_flag = True
            if set_ignoreF !=None and set_ignoreF == 0:
                ignore_flag = False
            if cnt[0] == 0:
                j = 0
                if(ignore_flag == True and (ii==0 or jj==0)):
                    skip = 'ok'
                else:
                    while(j<len(m1)):
                        if(file[i][1] == m1[j][1]):
                            del m1[j]
                        j = j + 1
                loop = 0
                append_ok = True
                while(loop<len(immcheck)):
                    if(immcheck[loop][0] == 0):
                        if(template[immcheck[loop][0]][immcheck[loop][1]]==file[i][5]):
                            append_ok = True
                        else:
                            append_ok = False
                    loop = loop + 1
                if(append_ok == True):            
                    m1.append(file[i])
                    m[0] = m1
                    cnt[0] = cnt[0] + 1
                else:
                    ok = 'ok'
            else:
                j = 0
                if(ignore_flag == True and (ii==0 or jj==0)):
                    skip = 'ok'
                else:
                    while(j<len(m[0])):
                        if(file[i][1] == m[0][j][1]):
                            del m[0][j]
                        j = j + 1
                loop = 0
                append_ok = True
                while(loop<len(immcheck)):
                    if(immcheck[loop][0] == 0):
                        if(template[immcheck[loop][0]][immcheck[loop][1]]==file[i][5]):
                            append_ok = True
                        else:
                            append_ok = False
                    loop = loop + 1
                if(append_ok == True):
                    m[0].append(file[i])
                else:
                    ok = 'ok'
        if(file[i][0] == template[1][0]):
            went_inside = True
            if set_ignoreT != None and set_ignoreT == 1:
                ignore_flag = True
               
            if set_ignoreF !=None and set_ignoreF == 1:
                ignore_flag = False
            if cnt[1] == 0:
                j = 0
                if(ignore_flag == True and (ii==1 or jj==1)):
                    skip = 'ok'
                else:
                    while(j<len(m2)):
                        if(file[i][1] == m2[j][1]):
                            del m2[j]
                        j = j + 1
                loop = 0
                append_ok = True
                while(loop<len(immcheck)):
                    if(immcheck[loop][0] == 1):
                        if(template[immcheck[loop][0]][immcheck[loop][1]]==file[i][5]):
                            append_ok = True
                        else:
                            append_ok = False
                    loop = loop + 1
                if(append_ok == True):            
                    m2.append(file[i])
                    m[1] = m2
                    cnt[1] = cnt[1] + 1
                else:
                    ok = 'ok'
            else:
                j = 0
                if(ignore_flag == True and (ii==1 or jj==1)):
                    skip = 'ok'
                else:
                    while(j<len(m[1])):
                        if(file[i][1] == m[1][j][1]):
                            del m[1][j]
                        j = j + 1
                loop = 0
                append_ok = True
                while(loop<len(immcheck)):
                    if(immcheck[loop][0] == 1):
                        if(template[immcheck[loop][0]][immcheck[loop][1]]==file[i][5]):
                            append_ok = True
                        else:
                            append_ok = False
                    loop = loop + 1
                if(append_ok == True):            
                    m[1].append(file[i])
                else:
                    ok = 'ok'
        if(file[i][0] == template[2][0]):
            went_inside = True
            if set_ignoreT != None and set_ignoreT == 2:
                ignore_flag = True
            if set_ignoreF !=None and set_ignoreF == 2:
                ignore_flag = False
            if cnt[2] == 0:
                j = 0
                if(ignore_flag == True and (ii==2 or jj==2)):
                    skip = 'ok'
                else:
                    while(j<len(m3)):
                        if(file[i][1] == m3[j][1]):
                            del m3[j]
                        j = j + 1
                loop = 0
                append_ok = True
                while(loop<len(immcheck)):
                    if(immcheck[loop][0] == 2):
                        if(template[immcheck[loop][0]][immcheck[loop][1]]==file[i][5]):
                            append_ok = True
                        else:
                            append_ok = False
                    loop = loop + 1
                if(append_ok == True):            
                    m3.append(file[i])
                    m[2] = m3
                    cnt[2] = cnt[2] + 1
                else:
                    ok = 'ok'
            else:
                j = 0
                if(ignore_flag == True and (ii==2 or jj==2)):
                    skip = 'ok'
                else:
                    while(j<len(m[2])):
                        if(file[i][1] == m[2][j][1]):
                            del m[2][j]
                        j = j + 1
                loop = 0
                append_ok = True
                while(loop<len(immcheck)):
                    if(immcheck[loop][0] == 2):
                        if(template[immcheck[loop][0]][immcheck[loop][1]]==file[i][5]):
                            append_ok = True
                        else:
                            append_ok = False
                    loop = loop + 1
                if(append_ok == True):            
                    m[2].append(file[i])
                else:
                    ok = 'ok'

       
        if(went_inside == False):
            k = 0
            while(k<len(m)):
                j = 0
                while(cnt[k] != 0 and j<len(m[k])):
                    
                    if(src1[0] == k and file[i][1] == m[src1[0]][j][src1[1]]):
                        del m[src1[0]][j]
                        j = j - 1
                    elif(src2[0] == k and file[i][1] == m[src2[0]][j][src2[1]]):
                        del m[src2[0]][j]
                        j = j - 1
                    elif(file[i][1] == m[k][j][1]):
                        del m[k][j]
                        j = j - 1
                    j = j + 1
                k = k + 1

        j=0
        k=0
        l=0

        flag1 = False
        flagcnt = False
        flagint = 0
        while(flagint<len(cnt)):
            if cnt[flagint] == 0:
                flagcnt = True
            flagint = flagint + 1

        while(flagcnt == False and j<len(m[2])):
            k = 0
            while(len(m[1])>0 and k<len(m[1]) and m[1][k][8]<m[2][j][8]):
                l = 0
                
                while(len(m[0])>0 and l<len(m[0]) and m[0][l][8]<m[1][k][8]):
                    
                    zz = 0
                    Flag = False
                    tt = []
                    tt.append(m[0][l])
                    tt.append(m[1][k])
                    tt.append(m[2][j])

                    while(zz<len(if_list1)):
                        if(tt[int(if_list1[zz])][int(if_list2[zz])] == tt[int(if_list3[zz])][int(if_list4[zz])]):
                            useless = 1
                        else:
                            Flag = True
                            break
                        zz = zz + 1
                    if(Flag == False):
                        print(m[0][l][8],m[1][k][8],m[2][j][8])
                        tt = []
                        tt.append(m[0][l])
                        tt.append(m[1][k])
                        tt.append(m[2][j])

                        temp_sum = 0
                        sum1 = 0
                        if(len(imm_check_sum)==2):
                            sum1 = sum1 + int(tt[imm_check_sum[temp_sum][0]][5])<<12
                            temp_sum = temp_sum + 1
                            sum1 = sum1 + int(tt[imm_check_sum[temp_sum][0]][5])
                        if((sum1<int('FFFFFFFF',16) and src2[0]==999) or (src2[0]!=999)):
                            replace = []
                            line_no1 = m[0][l][8]-1
                            line_no2 = m[1][k][8]-1
                            line_no3 = m[2][j][8]-1
                            replace.append(name)
                            replace.append(tt[dest[0]][dest[1]])
                            replace.append(tt[src1[0]][src1[1]])
                            if(src2[0]==999):
                                replace.append(str(sum1))
                            else:
                                replace.append(tt[src2[0]][src2[1]])
                            replace.append('None')
                            replace.append('None')
                            replace.append('None')
                            replace.append('None')
                            replace.append(line_no1+1)
                            replace.append(file[line_no3][9])
                            
                            
                            r = replace[0]+' '+replace[1]+','+replace[2]+','+replace[3]
                            rep = []
                            hexx = tt[0][9]
                            conv_str = str(hex(int(hexx)))
                            conv_str = conv_str[2:]
                            conv_str = conv_str[8:16]
                            pc_replace31.append(conv_str)
                            hexx = tt[1][9]
                            conv_str = str(hex(int(hexx)))
                            conv_str = conv_str[2:]
                            conv_str = conv_str[8:16]
                            pc_replace32.append(conv_str)
                            hexx = tt[2][9]
                            conv_str = str(hex(int(hexx)))
                            conv_str = conv_str[2:]
                            conv_str = conv_str[8:16]
                            pc_replace33.append(conv_str)
                            pc_replace34.append(r)


                            add = []
                            add.append(line_no1)
                            add.append(line_no2)
                            add.append(line_no3)
                            add.append(replace)
                            print(add)
                            opt_list3.append(add)
                            
                            ignore_flag = False
                            
                            rlno = line_no1
                            
                            del m[0][l]
                            del m[1][k]
                            del m[2][j]
                            if(set_ignoreF!=None):      #   Block 2
                                temp1 = tt[ii]          #
                                temp2 = tt[jj]          #
                                d = m[ii].index(temp2)  #   Deleting the repeating stored values in memory
                                del m[ii][d]            #
                                d = m[jj].index(temp1)  #
                                del m[jj][d]            #
                            flag1 = True
                            break
                    l = l + 1
                if flag1 == True:
                    break
                k = k + 1
            if flag1 == True:
                break
            j = j + 1
        i= i + 1
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def four_instruct(file,check_condition,template,name,dest,src1,src2):
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Same logic as three_instruct
    set_ignoreT = None
    set_ignoreF = None
    ii = 0
    jj = 0
    breakflag = False
    
    while(ii<len(template)):
        jj = ii + 1
        while(jj<len(template)):
            if(template[ii][0] == template[jj][0]):
                breakflag = True
                set_ignoreT = jj - 1
                set_ignoreF = jj + 1
                break
            jj = jj + 1
        if(breakflag==True):
            break
        ii = ii + 1
    
    immchi = 0
    immchj = 0
    immcheck = []
    imm_check_sum = []
    
    
    
    while(immchi<len(template)-1):
        tx = []
        if(template[immchi][0][len(template[immchi][0])-1]=='i'):
            imre = re.match('imm.*',template[immchi][len(template[immchi])-1])
            if(imre):
                donothing = 0
                tx.append(immchi)
                tx.append(len(template[immchi])-1)
                imm_check_sum.append(tx)
            else:
                tx.append(immchi)
                tx.append(len(template[immchi])-1)
                try:
                    int(template[tx[0]][tx[1]],16)
                except:
                    print('invalid immediate value in template',name)
                    return
                immcheck.append(tx)
        immchi = immchi + 1


    if_list1 = []
    if_list2 = []
    if_list3 = []
    if_list4 = []
    p = 0
    while(p<len(check_condition)):
        temp1 = check_condition[p].split(';')
        temp11 = str(temp1[0]).split(',')
        temp12 = str(temp1[1]).split(',')
        if_list1.append(temp11[0])
        if_list2.append(temp11[1])
        if_list3.append(temp12[0])
        if_list4.append(temp12[1])
        p = p + 1

    print(name)

    i = 0
    cnt1 = 0
    cnt2 = 0
    m1 = []
    m2 = []
    m3 = []
    m4 = []
    cnt = []
    cnt.append(0)
    cnt.append(0)
    cnt.append(0)
    cnt.append(0)
    m = [None,None,None,None]
    went_inside = False
    ignore_flag = False
    while(i<len(file)):
        went_inside = False
        if(file[i][0] == template[0][0]):
            went_inside = True
            if set_ignoreT != None and set_ignoreT == 0:
                ignore_flag = True
            if set_ignoreF !=None and set_ignoreF == 0:
                ignore_flag = False
            if cnt[0] == 0:
                j = 0
                if(ignore_flag == True and (ii==0 or jj==0)):
                    skip = 'ok'
                else:
                    while(j<len(m1)):
                        if(file[i][1] == m1[j][1]):
                            del m1[j]
                        j = j + 1
                loop = 0
                append_ok = True
                while(loop<len(immcheck)):
                    if(immcheck[loop][0] == 0):
                        if(template[immcheck[loop][0]][immcheck[loop][1]]==file[i][5]):
                            append_ok = True
                        else:
                            append_ok = False
                    loop = loop + 1
                if(append_ok == True):            
                    m1.append(file[i])
                    m[0] = m1
                    cnt[0] = cnt[0] + 1
                else:
                    ok = 'ok'
            else:
                j = 0
                if(ignore_flag == True and (ii==0 or jj==0)):
                    skip = 'ok'
                else:
                    while(j<len(m[0])):
                        if(file[i][1] == m[0][j][1]):
                            del m[0][j]
                        j = j + 1
                loop = 0
                append_ok = True
                while(loop<len(immcheck)):
                    if(immcheck[loop][0] == 0):
                        if(template[immcheck[loop][0]][immcheck[loop][1]]==file[i][5]):
                            append_ok = True
                        else:
                            append_ok = False
                    loop = loop + 1
                if(append_ok == True):
                    m[0].append(file[i])
                else:
                    ok = 'ok'
        if(file[i][0] == template[1][0]):
            went_inside = True
            if set_ignoreT != None and set_ignoreT == 1:
                ignore_flag = True
            if set_ignoreF !=None and set_ignoreF == 1:
                ignore_flag = False
            if cnt[1] == 0:
                j = 0
                if(ignore_flag == True and (ii==1 or jj==1)):
                    skip = 'ok'
                else:
                    while(j<len(m2)):
                        if(file[i][1] == m2[j][1]):
                            del m2[j]
                        j = j + 1
                loop = 0
                append_ok = True
                while(loop<len(immcheck)):
                    if(immcheck[loop][0] == 1):
                        if(template[immcheck[loop][0]][immcheck[loop][1]]==file[i][5]):
                            append_ok = True
                        else:
                            append_ok = False
                    loop = loop + 1
                if(append_ok == True):            
                    m2.append(file[i])
                    m[1] = m2
                    cnt[1] = cnt[1] + 1
                else:
                    ok = 'ok'
                
            else:
                j = 0
                if(ignore_flag == True and (ii==1 or jj==1)):
                    skip = 'ok'
                else:
                    while(j<len(m[1])):
                        if(file[i][1] == m[1][j][1]):
                            del m[1][j]
                        j = j + 1
                loop = 0
                append_ok = True
                while(loop<len(immcheck)):
                    if(immcheck[loop][0] == 1):
                        if(template[immcheck[loop][0]][immcheck[loop][1]]==file[i][5]):
                            append_ok = True
                        else:
                            append_ok = False
                    loop = loop + 1
                if(append_ok == True):            
                    m[1].append(file[i])
                else:
                    ok = 'ok'
                
        if(file[i][0] == template[2][0]):
            went_inside = True
            if set_ignoreT != None and set_ignoreT == 2:
                ignore_flag = True
            if set_ignoreF !=None and set_ignoreF == 2:
                ignore_flag = False
            if cnt[2] == 0:
                j = 0
                if(ignore_flag == True and (ii==2 or jj==2)):
                    skip = 'ok'
                else:
                    while(j<len(m3)):
                        if(file[i][1] == m3[j][1]):
                            del m3[j]
                        j = j + 1
                loop = 0
                append_ok = True
                while(loop<len(immcheck)):
                    if(immcheck[loop][0] == 2):
                        if(template[immcheck[loop][0]][immcheck[loop][1]]==file[i][5]):
                            append_ok = True
                        else:
                            append_ok = False
                    loop = loop + 1
                if(append_ok == True):            
                    m3.append(file[i])
                    m[2] = m3
                    cnt[2] = cnt[2] + 1
                else:
                    ok = 'ok'    
            else:
                j = 0
                if(ignore_flag == True and (ii==2 or jj==2)):
                    skip = 'ok'
                else:
                    while(j<len(m[2])):
                        if(file[i][1] == m[2][j][1]):
                            del m[2][j]
                        j = j + 1
                loop = 0
                append_ok = True
                while(loop<len(immcheck)):
                    if(immcheck[loop][0] == 2):
                        if(template[immcheck[loop][0]][immcheck[loop][1]]==file[i][5]):
                            append_ok = True
                        else:
                            append_ok = False
                    loop = loop + 1
                if(append_ok == True):            
                    m[2].append(file[i])
                else:
                    ok = 'ok'
                
        if(file[i][0] == template[3][0]):
            went_inside = True
            if set_ignoreT != None and set_ignoreT == 3:
                ignore_flag = True
            if set_ignoreF !=None and set_ignoreF == 3:
                ignore_flag = False
            if cnt[3] == 0:
                j = 0
                if(ignore_flag == True and (ii==3 or jj==3)):
                    skip = 'ok'
                else:
                    while(j<len(m4)):
                        if(file[i][1] == m4[j][1]):
                            del m4[j]
                        j = j + 1
                
                loop = 0
                append_ok = True
                while(loop<len(immcheck)):
                    if(immcheck[loop][0] == 3):
                        if(template[immcheck[loop][0]][immcheck[loop][1]]==file[i][5]):
                            append_ok = True
                        else:
                            append_ok = False
                    loop = loop + 1
                if(append_ok == True):            
                    m4.append(file[i])
                    m[3] = m4
                    cnt[3] = cnt[3] + 1
                else:
                    ok = 'ok' 
            else:
                j = 0
                if(ignore_flag == True and (ii==3 or jj==3)):
                    skip = 'ok'
                else:
                    while(j<len(m[3])):
                        if(file[i][1] == m[3][j][1]):
                            del m[3][j]
                        j = j + 1
                
                loop = 0
                append_ok = True
                while(loop<len(immcheck)):
                    if(immcheck[loop][0] == 3):
                        if(template[immcheck[loop][0]][immcheck[loop][1]]==file[i][5]):
                            append_ok = True
                        else:
                            append_ok = False
                    loop = loop + 1
                if(append_ok == True):            
                    m[3].append(file[i])
                else:
                    ok = 'ok'
                

        if(went_inside == False):
            k = 0
            while(k<len(m)):
                j = 0
                while(cnt[k] != 0 and j<len(m[k])):
                    if(src1[0] == k and file[i][1] == m[src1[0]][j][src1[1]]):
                        del m[src1[0]][j]
                        j = j - 1
                    elif(src2[0] == k and file[i][1] == m[src2[0]][j][src2[1]]):
                        del m[src2[0]][j]
                        j = j - 1
                    elif(file[i][1] == m[k][j][1]):
                        del m[k][j]
                        j = j - 1
                    j = j + 1
                k = k + 1

        h=0
        j=0
        k=0
        l=0

        flag1 = False
        flagcnt = False
        flagint = 0
        while(flagint<len(cnt)):
            if cnt[flagint] == 0:
                flagcnt = True
            flagint = flagint + 1

        while(flagcnt == False and h<len(m[3])):
            j = 0
            while(len(m[2])>0 and j<len(m[2]) and m[2][j][8]<m[3][h][8]):
                k = 0
                while(len(m[1])>0 and k<len(m[1]) and m[1][k][8]<m[2][j][8]):
                    l = 0
                    
                    while(len(m[0])>0 and l<len(m[0]) and m[0][l][8]<m[1][k][8]):
                    
                    
                        zz = 0
                        Flag = False
                        tt = []
                        tt.append(m[0][l])
                        tt.append(m[1][k])
                        tt.append(m[2][j])
                        tt.append(m[3][h])

                        while(zz<len(if_list1)):
                            if(tt[int(if_list1[zz])][int(if_list2[zz])] == tt[int(if_list3[zz])][int(if_list4[zz])]):
                                useless = 1
                            else:
                                Flag = True
                                break
                            zz = zz + 1
                        if(Flag == False):
                            print(m[0][l][8],m[1][k][8],m[2][j][8],m[3][h][8])
                            tt = []
                            tt.append(m[0][l])
                            tt.append(m[1][k])
                            tt.append(m[2][j])
                            tt.append(m[3][h])

                            temp_sum = 0
                            sum1 = 0

                            if(len(imm_check_sum)==2):
                                sum1 = sum1 + int(tt[imm_check_sum[temp_sum][0]][5])<<12
                                temp_sum = temp_sum + 1
                                sum1 = sum1 + int(tt[imm_check_sum[temp_sum][0]][5])
                            if((sum1<int('FFFFFFFF',16) and src2[0]==999) or (src2[0]!=999)):
                                
                                replace = []
                                line_no1 = m[0][l][8]-1
                                line_no2 = m[1][k][8]-1
                                line_no3 = m[2][j][8]-1
                                line_no4 = m[3][h][8]-1
                                replace.append(name)
                                replace.append(tt[dest[0]][dest[1]])
                                replace.append(tt[src1[0]][src1[1]])
                                if(src2[0]==999):
                                    replace.append(str(sum1))
                                else:
                                    replace.append(tt[src2[0]][src2[1]])
                                replace.append('None')
                                replace.append('None')
                                replace.append('None')
                                replace.append('None')
                                replace.append(line_no1+1)
                                replace.append(file[line_no4][9])

                                

                                
                                
                                r = replace[0]+' '+replace[1]+','+replace[2]+','+replace[3]
                                rep = []
                                hexx = tt[0][9]
                                conv_str = str(hex(int(hexx)))
                                conv_str = conv_str[2:]
                                conv_str = conv_str[8:16]
                                pc_replace41.append(conv_str)
                                hexx = tt[1][9]
                                conv_str = str(hex(int(hexx)))
                                conv_str = conv_str[2:]
                                conv_str = conv_str[8:16]
                                pc_replace42.append(conv_str)
                                hexx = tt[2][9]
                                conv_str = str(hex(int(hexx)))
                                conv_str = conv_str[2:]
                                conv_str = conv_str[8:16]
                                pc_replace43.append(conv_str)
                                hexx = tt[3][9]
                                conv_str = str(hex(int(hexx)))
                                conv_str = conv_str[2:]
                                conv_str = conv_str[8:16]
                                pc_replace44.append(conv_str)
                                pc_replace45.append(r)
                                
                                
                                
                                add = []
                                add.append(line_no1)
                                add.append(line_no2)
                                add.append(line_no3)
                                add.append(line_no4)
                                add.append(replace)
                                print(add)

                                opt_list4.append(add)

                                ignore_flag = False
                                
                                del m[0][l]
                                del m[1][k]
                                del m[2][j]
                                del m[3][h]
                                if(set_ignoreF!=None):
                                    temp1 = tt[ii]
                                    temp2 = tt[jj]
                                    d = m[ii].index(temp2)
                                    del m[ii][d]
                                    d = m[jj].index(temp1)
                                    del m[jj][d]
                                flag1 = True
                                break
                        l = l + 1
                    if flag1 == True:
                        break
                    k = k + 1
                if flag1 == True:
                    break
                j = j + 1
            if flag1 == True:
                break
            h = h + 1
        i= i + 1
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_disass(dissass_file,modified_disass_path,mode):
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Code parse the disassembly file and create .S file
    mode = None
    dissass = open(dissass_file,'r')
    Path(modified_disass_path).mkdir(parents=True, exist_ok=True)
    modif_disass_file = modified_disass_path+'/'+os.path.basename(dissass_file)[:-6]+'s'
    Path(modif_disass_file).touch(mode=0o777, exist_ok=True)
    outcheck = open(modif_disass_file,'w') # --> standard output file 
    addlist = []
    in_mem = []
    in_mempc = []
    while(1):
        line = dissass.readline()
        newline = []
        line2 = ''
        if not line:
            break
        
        z1 = re.match('.*<.*>:',line)
        z2 = re.match('.* \.data:',line) # --> To stop parsing if data segment is found
        if z1:
            d1 = line.index('<') + 1
            d2 = line.index('>')
            ad = []
            strk = line[d1:d2]
            strk1 = line[8:16]
            print(strk1)
            ad.append(strk)
            ad.append(strk1)
            addlist.append(ad)
            k = line[d1:d2].replace('-','_')+':\n'
            in_mem.append(k)
            in_mempc.append(None)
        if z2:
            break
        else:
            pnewline = line
            line = line + ': '
            line = line.split(':')
            
            line[1] = line[1].replace('\t',' ')
            line[1] = line[1].replace('\n',' ')
            line[0] = line[0].replace('\n',' ')
            line[1] = line[1].split(' ')
            line[0] = line[0].split(' ')
            i = 0
            while(i<len(line[0])):
                if(line[0][i] != ''):
                    newline.append(line[0][i])
                    break
                i = i + 1
            
            i = 0
            while(i<len(line[1])):
                if(line[1][i] != ''):
                    line2 = line2 +' '+ line[1][i]
                i = i + 1
            line2 = line2.split(' ')
            line3 = []
            i = 0
            while(i<len(line2)):
                if(line2[i] != ''):
                    line3.append(line2[i])
                i = i + 1
            line4 = ''
            i = 1
            while(i<len(line3)):
                line4 = line4 + ' ' + line3[i]
                i = i + 1

            if (len(line3)==0):
                line3.append(' ')
            newline.append(line3[0])
            newline.append(line4)
            
            skip = False
            if(len(newline)==3):
                pnewline = 'core   0: 0x'+newline[0]+' '+'(0x'+newline[1]+')'+' '+newline[2]
                try:
                    instr = rita.parsers.parseInstruction(pnewline, mode)
                    if instr is not None:
                        if newline[0] in pc_replace41:
                            skip = True
                            in_mem.append('nop #addedbyrita!@#$\n') # --> writing nop before adding labels 
                            in_mempc.append(newline[0])
                            
                        elif newline[0] in pc_replace42:
                            skip = True
                            in_mem.append('nop #addedbyrita!@#$\n') # --> writing nop before adding labels 
                            in_mempc.append(newline[0])
                            
                        elif newline[0] in pc_replace43:
                            skip = True
                            in_mem.append('nop #addedbyrita!@#$\n') # --> writing nop before adding labels 
                            in_mempc.append(newline[0])
                            
                        elif newline[0] in pc_replace44:
                            d = pc_replace44.index(newline[0])
                            newline[2] = pc_replace45[d]
                            
                        elif newline[0] in pc_replace31:
                            skip = True
                            in_mem.append('nop #addedbyrita!@#$\n') # --> writing nop before adding labels 
                            in_mempc.append(newline[0])
                        elif newline[0] in pc_replace32:
                            skip = True
                            in_mem.append('nop #addedbyrita!@#$\n') # --> writing nop before adding labels 
                            in_mempc.append(newline[0])
                        elif newline[0] in pc_replace33:
                            d = pc_replace33.index(newline[0])
                            newline[2] = pc_replace34[d]
                        elif newline[0] in pc_replace21:
                            skip = True
                            in_mem.append('nop #addedbyrita!@#$\n') # --> writing nop before adding labels 
                            in_mempc.append(newline[0])
                        elif newline[0] in pc_replace22:
                            
                            d = pc_replace22.index(newline[0])
                            newline[2] = pc_replace23[d]
                            
                        if(skip == False):
                            newline[2] = newline[2].replace('<',';#<')
                            in_mem.append(newline[2]+'\n')
                            in_mempc.append(newline[0])
                except:
                    print('Error while parsing :',pnewline)
    
    i = 0
    while(i<len(in_mem)):
        mnm = 0
        while(mnm<len(addlist)):
            if ':' not in in_mem[i]:
                in_mem[i] = in_mem[i].replace(addlist[mnm][1],addlist[mnm][0])
            mnm = mnm + 1
        i = i + 1
    i = 0
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# replacing hex addresses with lables
    while(i<len(in_mem)):
        temp = in_mem[i].split(' ')
        if(len(temp)>=3):
            _2branch = ['beqz','bnez','blez','bgez','bltz','bgtz','jal'] 
            _3branch = ['bgt','ble','bgtu','bleu','beq','bne','bge','blt','bltu','bgeu']
            _1branch = ['j']
            if(temp[1] in _1branch):
                temp[2] = temp[2].replace('\n','')
                save = temp[2]
                try:
                    p = str(hex(int(save,16)))
                    save = save.replace(' ','')
                    save = save.replace('\n','')

                    if(len(save)==8):
                        if(save in in_mempc):
                            d = in_mempc.index(save)
                            str1 = 'label'+str(i)+':\n'
                            strrep = 'label'+str(i)+'\n'
                            in_mem[i]=in_mem[i].replace(save,strrep)
                            in_mem.insert(d,str1)
                            
                        else:
                            save1 = '0x'+save
                            im_mem[i]=in_mem[i].replace(save,save1)
                except:
                    donothing = 1
                if(in_mem[i][len(in_mem[i])-1] != '\n'):
                    in_mem[i] = in_mem[i] + '\n'

            if(temp[1] in _2branch):
                temp[2] = temp[2].replace('\n','')
                temp2 = temp[2].split(',')
                save = temp2[1]
                
                try:
                    p = str(hex(int(save,16)))
                    save = save.replace(' ','')
                    save = save.replace('\n','')

                    if(len(save)==8):
                        
                        if(save in in_mempc):
                            d = in_mempc.index(save)
                            str1 = 'label'+str(i)+':\n'
                            strrep = 'label'+str(i)+'\n'
                            in_mem[i]=in_mem[i].replace(save,strrep)
                            in_mem.insert(d,str1)
                            
                        else:
                            save1 = '0x'+save
                            im_mem[i]=in_mem[i].replace(save,save1)
                except:
                    donothing = 1
                if(in_mem[i][len(in_mem[i])-1] != '\n'):
                    in_mem[i] = in_mem[i] + '\n'
            if(temp[1] in _3branch):
                
                temp[2] = temp[2].replace('\n','')
                temp2 = temp[2].split(',')
                save = temp2[2]
                
                try:
                    p = str(hex(int(save,16)))
                    save = save.replace(' ','')
                    save = save.replace('\n','')

                    if(len(save)==8):
                        if(save in in_mempc):
                            d = in_mempc.index(save)
                            str1 = 'label'+str(i)+':\n'
                            strrep = 'label'+str(i)+'\n'
                            in_mem[i]=in_mem[i].replace(save,strrep)
                            in_mem.insert(d,str1)
                            
                        else:
                            save1 = '0x'+save
                            im_mem[i]=in_mem[i].replace(save,save1)
                except:
                    donothing = 1
                if(in_mem[i][len(in_mem[i])-1] != '\n'):
                    in_mem[i] = in_mem[i] + '\n'
        i = i+1
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    i = 0
    while(i<len(in_mem)):
        
        ch = re.match('.*addedbyrita\!\@\#\$',in_mem[i]) # --> ignoring all nops added by RITA
        if(ch):
            k = 'k'
        else:
            outcheck.write(in_mem[i])
        i = i + 1
    outcheck.close()
        



# Start of program execution, compute is called by main.py
def compute(input_file, template_file, disass_file, modif_disass_path, out_file, args):
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Creating a copy of the input template file and naming it template.py to be used by `Statement - 1`
    if(template_file!='template.py'):
        f = open(template_file,'r')
        f1 = open('template.py','w+')
        while(1):
            line = f.readline()
            if not line:
                break
            f1.write(line)
        f1.close()

    from template  import TEMPLATE # --> Statement - 1
    check_condition = []
    check_conditions = []
    templates = []
    template = []
    aa = []
    temp2 = None
    temp3 = None
    i = 0
    list = []
    for i in TEMPLATE.keys():
        list.append(i)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# For converting each template into matrix format 
    i = 0
    while(i<len(TEMPLATE)):
        temptemp = list[i].split(' ')
        jc = []
        jc.append(temptemp[0])
        ab = temptemp[1].split(',')
        for k in ab:
            jc.append(k)
        temp2 = jc
        j = 0
        while(j<len(TEMPLATE[list[i]])):
            jc = []
            temptemp = TEMPLATE[list[i]][j].split(' ')
            jc.append(temptemp[0])
            ab = temptemp[1].split(',')
            for k in ab:
                jc.append(k)
            template.append(jc)
            j = j + 1
        template.append(temp2)
        templates.append(template)
        template = []
        i = i + 1
    os.remove('template.py')
# Check by printing variable templates
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Checking if the instructions in the template contain any invalid instructions
    acceptable_list = ['addi','xori','ori','andi','slli','srli','srai','add','sub','sll','slt','sltu','xor','srl','sra','or','and','lui']
    ti = 0
    tj = 0
    while(ti<len(templates)):
        tj = 0
        while(tj<len(templates[ti])-1):
            if(templates[ti][tj][0] not in acceptable_list):
                print('invalid template',ti+1,' instruction \'',templates[ti][tj][0],'\' not found')
                exit(0)
            tj = tj + 1
        ti = ti + 1
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# All dependencies exist withing two dimensional space (e.g Dest of instruction 2 ---> Source 1 of instruction 3)
# Code to find the indices of the template matrix elements that are same
    i = 0
    while(i<len(templates)):
        j = 0
        while(j<len(templates[i])-1):
            k = j + 1
            while(k<len(templates[i])-1):
                l = 1
                d = 0
                kk = 0
                temp = []
                while(kk<len(templates[i][k])):
                    temp.append(templates[i][k][kk])
                    kk = kk + 1
                ignore = False
                while(l<len(templates[i][j])):
                    if(templates[i][j][l] in temp and templates[i][j][l]!='\n'):
                        m = temp.index(templates[i][j][l])
                        z = m
                        while(z>=0):
                            del temp[0]
                            z = z - 1 
                        d = d + m 
                        check_condition.append(str(j)+','+str(l)+';'+str(k)+','+str(d))
                        ignore = True
                    else:
                        ignore = False
                    if(ignore == False):    
                        l = l + 1
                        kk = 0
                        d = 0
                        temp = []
                        while(kk<len(templates[i][k])):
                            temp.append(templates[i][k][kk])
                            kk = kk + 1
                k = k + 1
            j = j + 1
        check_conditions.append(check_condition)
        if(check_condition == []):
            print('No dependency, Error, Template:',i+1 )
            exit()
        check_condition = []
        i = i + 1

    print('Templates Collected!')
# Check by printing variable check_conditions
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    cnt = Counter()
    total_insts = 0
    output_size = None if args.output_size is None else args.output_size
    addr_min = None if args.addr_min is None else args.addr_min
    addr_max = None if args.addr_max is None else args.addr_max
    mode = args.mode
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Code to read RITA LOG and store in matrix format in memory with 9 columns
    kkk = 1
    with open(input_file) as fp:
        for line in fp:
            instr = rita.parsers.parseInstruction(line, mode)

            if instr is None:
                print("Skipping {0}".format(line), file=sys.stderr)
                continue

            instr_name = instr.instr_name
            rd = instr.rd
            rs1 = instr.rs1
            rs2 = instr.rs2
            rs3 = instr.rs3
            imm = instr.imm
            csr = instr.csr
            shamt = instr.shamt
            instr_addr = instr.instr_addr
            if instr_name is None:
             instr_name = "None"
            if rd is None:
             rdstr="None"
            else:
             rdstr=str(rd[1])+str(rd[0])
            if rs1 is None:
             rs1str="None"
            else:
             rs1str=str(rs1[1])+str(rs1[0])
            if rs2 is None:
             rs2str="None"
            else:
             rs2str=str(rs2[1])+str(rs2[0])
            if rs3 is None:
             rs3str="None"
            else:
             rs3str=str(rs3[1])+str(rs3[0])
            if imm is None:
             immstr="None"
            else:
             immstr=str(imm)
            if csr is None:
             csrstr="None"
            else:
             csrstr=str(csr)
            if shamt is None:
             shamtstr="None"
            else:
             shamtstr=str(shamt)
            if instr_addr is None:
             instr_addrstr = "None"
            else:
             instr_addrstr = str(instr_addr)


            iinst = []
            file1.append(instr_name+' '+rdstr+' '+rs1str+' '+rs2str+' '+rs3str+' '+immstr+' '+csrstr+' '+shamtstr)
            iinst.append(instr_name)
            iinst.append(rdstr)
            iinst.append(rs1str)
            iinst.append(rs2str)
            iinst.append(rs3str)
            iinst.append(immstr)
            iinst.append(csrstr)
            iinst.append(shamtstr)
            iinst.append(kkk)
            iinst.append(instr_addrstr)
            file.append(iinst)
            kkk = kkk + 1
# Check by printing variable file
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Code to find the indices of source1, source2/immediate and destination in template
    i = 0
    while(i<len(templates)):
        line = templates[i][len(templates[i]) - 1]
        m = 0
        dest = []
        src1 = []
        src2 = []
        while(m<len(templates[i])-1):
            if line[1] in templates[i][m]:
                d = templates[i][m].index(line[1])
                dest.append(m)
                dest.append(d)
                break
            m = m + 1
        m = 0

        while(m<len(templates[i])-1):
            if line[2] in templates[i][m]:
                d = templates[i][m].index(line[2])
                src1.append(m)
                src1.append(d)
                break
            m = m + 1
        m = 0
        while(m<len(templates[i])-1):
            if line[3] == 'immd': #
                src2.append(999)  # --> Specific code for templates along lines of load32 with immd = (imm1 << 12) | imm2
                src2.append(999)  #
                break
            if line[3] in templates[i][m]:
                d = templates[i][m].index(line[3])
                src2.append(m)
                src2.append(d)
                break
            m = m + 1
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if len(templates[i])==3:
            two_instruct(file,check_conditions[i],templates[i],line[0],dest,src1,src2)
        if len(templates[i])==4:
            three_instruct(file,check_conditions[i],templates[i],line[0],dest,src1,src2)
        if len(templates[i])==5:
            four_instruct(file,check_conditions[i],templates[i],line[0],dest,src1,src2)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        i = i + 1
    rep_two(file,opt_list2)   #-->shift right by 1 tab if improper out-file
    rep_three(file,opt_list3) #                 "
    rep_four(file,opt_list4)  #                 "
    line_no_correct(file)     #                 "
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Code for writing into intermediate out file (i.e. changed spike output)
    check = 0
    f1 = open(out_file,'w+')
    f1.write('Inst dest src1 src2 src3 imm csr shamt addr \n')
    while(check<len(file)):
        line = file[check][0]+' '+file[check][1]+' '+file[check][2]+' '+file[check][3]+' '+file[check][4]+' '+file[check][5]+' '+file[check][6]+' '+file[check][7]+' '+file[check][9]+'\n'
        
        f1.write(line)
        check = check + 1
    f1.close()
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    i = 0
    while(i<len(pc_replace41)):
        print(pc_replace41[i],pc_replace42[i],pc_replace43[i],pc_replace44[i],pc_replace45[i])
        i = i + 1
    parse_disass(disass_file,modif_disass_path,mode)
    
    
