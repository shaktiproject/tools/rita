import rita.parsers
from collections import Counter
import sys

'''Register histrogram post-processing module'''

help_message = '{:<20s} {:<10s}'.format('hist_reg', 'Histogram of register usage')

def compute(input_file, args):
    '''Compute the histogram'''
    cnt_dst = Counter()
    cnt_src = Counter()
    total_insts = 0
    output_size = None if args.output_size is None else args.output_size
    addr_min = None if args.addr_min is None else args.addr_min
    addr_max = None if args.addr_max is None else args.addr_max
    mode = args.mode

    with open(input_file) as fp:
        for line in fp:
            instr = rita.parsers.parseInstruction(line, mode)
            if instr is None:
                print("Skipping {0}".format(line), file=sys.stderr)
                continue

            addr = instr.instr_addr
            if addr_min is not None and addr < addr_min:
                continue
            if addr_max is not None and addr > addr_max:
                break

            if instr.rd is not None:
                reg_name = instr.rd[1] + '{:02d}'.format(instr.rd[0])
                cnt_dst[reg_name] += 1
            if instr.rs1 is not None:
                reg_name = instr.rs1[1] + '{:02d}'.format(instr.rs1[0])
                cnt_src[reg_name] += 1
            if instr.rs2 is not None:
                reg_name = instr.rs2[1] + '{:02d}'.format(instr.rs2[0])
                cnt_src[reg_name] += 1
            if instr.rs3 is not None:
                reg_name = instr.rs3[1] + '{:02d}'.format(instr.rs3[0])
                cnt_src[reg_name] += 1
            #TODO: Add the CSR stat

            total_insts += 1

    print("Histogram of Register Usage\n")
    print("  Total number of instructions: {0:d}\n".format(total_insts))

    print("  Integer Register Statistics")
    print("  ---------------------------")
    print("  {0:<10s} {1:<10s} {2:<10s} {3:<10s} {4:<10s}".format("Name", "Reads", "Read %", "Writes", "Writes %"))
    print("  --------------------------------------------------")

    for i in range(32):
        reg_name = 'x' + '{:02d}'.format(i)
        read_cnt = cnt_src[reg_name]
        write_cnt = cnt_dst[reg_name]
        read_cnt_p = str('{:05.2f}'.format(100*read_cnt/total_insts))
        write_cnt_p = str('{:05.2f}'.format(100*write_cnt/total_insts))

        print('  {0:<9s} {1:<10s} {2:<10s} {3:<10s} {4:<10s}'.format(reg_name, str(read_cnt), read_cnt_p, str(write_cnt), write_cnt_p))

    print("\n  Floating Point Register Statistics")
    print("  -----------------------------------")
    print("  {0:<10s} {1:<10s} {2:<10s} {3:<10s} {4:<10s}".format("Name", "Reads", "Read %", "Writes", "Writes %"))
    print("  --------------------------------------------------")
    for i in range(32):
        reg_name = 'f' + '{:02d}'.format(i)
        read_cnt = cnt_src[reg_name]
        write_cnt = cnt_dst[reg_name]
        read_cnt_p = str('{:05.2f}'.format(100*read_cnt/total_insts))
        write_cnt_p = str('{:05.2f}'.format(100*write_cnt/total_insts))

        print('  {0:<9s} {1:<10s} {2:<10s} {3:<10s} {4:<10s}'.format(reg_name, str(read_cnt), read_cnt_p, str(write_cnt), write_cnt_p))
