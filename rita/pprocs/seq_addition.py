import rita.parsers
from collections import Counter
#from prettytable import PrettyTable
import sys
f=open("instruct","w")
'''Histogram post-processing module'''
#t = PrettyTable(['Name', 'RD','RS1','RS2','RS3','imm','CSR','shamt'])
help_message = '{:<20s} {:<10s}'.format('hist_instr', 'Histogram of instructions')

def compute(input_file, args):
    '''Compute the histogram'''
    cnt = Counter()
    total_insts = 0
    output_size = None if args.output_size is None else args.output_size
    addr_min = None if args.addr_min is None else args.addr_min
    addr_max = None if args.addr_max is None else args.addr_max
    mode = args.mode
  
    with open(input_file) as fp:
        for line in fp:
            instr = rita.parsers.parseInstruction(line, mode)

            if instr is None:
                print("Skipping {0}".format(line), file=sys.stderr)
                continue

            #addr = instr.instr_addr
            instr_name = instr.instr_name
            rd = instr.rd
            rs1 = instr.rs1
            rs2 = instr.rs2
            rs3 = instr.rs3
            imm = instr.imm
            csr = instr.csr
            shamt = instr.shamt
            if instr_name is None:
             instr_name = "None"
            if rd is None:
             rdstr="None"
            else:
             rdstr=str(rd[1])+str(rd[0])
            if rs1 is None:
             rs1str="None"
            else:
             rs1str=str(rs1[1])+str(rs1[0])
            if rs2 is None:
             rs2str="None"
            else:
             rs2str=str(rs2[1])+str(rs2[0])
            if rs3 is None:
             rs3str="None"
            else:
             rs3str=str(rs3[1])+str(rs3[0])
            if imm is None:
             immstr="None"
            else:
             immstr=str(imm)
            if csr is None:
             csrstr="None"
            else:
             csrstr=str(csr)
            if shamt is None:
             shamtstr="None"
            else:
             shamtstr=str(shamt)
            #print(instr_name,'\t',rd,'\t',rs1,'\t',rs2,'\t',rs3,'\t',imm,'\t',csr,'\t',shamt)
            #t.add_row([instr_name,rd,rs1,rs2,rs3,imm,csr,shamt])
            f.write(instr_name+'\t'+rdstr+'\t'+rs1str+'\t'+rs2str+'\t'+rs3str+'\t'+immstr+'\t'+csrstr+'\t'+shamtstr+'\n')
           # if addr_min is not None and addr < addr_min:
               # continue
           # if addr_max is not None and addr > addr_max:
               # break

            if instr.instr_name is not None:
                cnt[instr.instr_name] += 1
                total_insts += 1
    #print(t)
    f.close()
    print('finished writing into instruct file \n')
   # print("Histogram of Instructions\n")
   # print("  Total number of instructions: {0:d}\n".format(total_insts))
   # print("  {0:<10s} {1:<10s} {2:<10s}".format("Name", "Frequency", "Percentage"))
   # print("  -------------------------------------")

    #for instr in cnt.most_common(output_size):
       #print("  {0:<10s} {1:<10} {2:<10.2f}".format(instr[0], instr[1], 100*instr[1]/total_insts))
