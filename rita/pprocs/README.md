RITA - Sequence Extraction
--------------------------
1. Intoduction
-----------
This branch of RITA is serves to identify occourences of sequences of instructions based on templates (specifically bitmanipulation), aimed at the reduction of code density, with an additional provision to create an 'optimised' assembly file based on a given disassembly file corresponding to the input logfile.
The basic requirements to utilise this feature of rita are :
* python3
* logfile (usually spike output)
* template file (syntax mentioned in section 2.)

	The Log file serves as an input to this feature of rita, wich demultiplexes the opcode to obtain information about the state of each register during the execution of the program. Because this is a trace of the program execution, the information obtained from this helps in understanding the number of times a template, if present, is invoked/executed. However the user is still benighted on the measure to which code density is optimised when the sequence of instructions under consideration are replaced by the single instruction as mentioned in the template.
	Thus the additional feature of RITA, comes into the picture, which creates a .S(assembly level file), which on execution can give us a clearer picture on the changes/improvement w.r.t code-density

Template file Syntax and Sample
-------------------------------
With the example given below, one can clearly understand the syntax to be adhered to while creation of the Template file.
```
TEMPLATE = {
    'andn dest,src1,src2':[
        'xori dep1,src2,imm1',
        'and dest,dep1,src1'
    ],
    'sro dest,src1,src2':[
        'andi dep1,src2,63',
        'xori dep2,src1,-1',
        'srl dep3,dep4,dep5',
        'xori dest,src,-1'
    ]}
```
Points to remember while generating template files
* The template file, is treated as a dict by the sequence extraction program, where the replacing instructions serve as individual keys, which in turn point to an abstract 'list' containing the instructions to be replaced. The take away here is that because each instruction to be replaced belongs to the list in the form of strings, they are parsed as such, thus there exist syntactic differences between `xori dep1,src2,imm1` and `xori dep1, src2, imm1` where the latter is considered to be of an invalid syntax.

* The destination src1 and src2/imm in the replacing instruction my mandatorily be present in the template. The program will also gracefully exit incase there is an absence of dependencies bewteen instructions of a template or the instructions belonging to a template are invalid

* Understanding the minute nuances in the example given above can open a wide array of flexible options. Consider the immediate field, with one template mentioning it as imm1 while the other as a number itself. This program does not hard check the immediates in the template that can be matched against the regular expression `imm.*`. However if an explict number is mentioned, the template yeilds results if and only if the immediate number given in the template, also occours while execution

* In the case of instructions along the lines of load32, a special provision has been made to check calculate the immediate value of the replacing instruction, iff two conditions are satisfied -
	The sum is within acceptable bounds and there are exactly 2 instructions involving immediates in the template.
	The destination must be named `immd` (signifying that it is the destination of the sum of the 2 other immediates after shifting the first one left by 12) [`immd = (imm1 << 12) | imm2`], and only these can violate the requiremet for the src2 of the replacing instruction to be a part of the template

The output .S (modified Disassembly) file is named whatever the `disass file` is named and can be found at `modif-disass-path` argument .

The Seq Extraction Feature Can be summarised as

![](./seq-extr.png)

An example rita call:
```bash
python3 -m rita.main --trace-file spike-demo.dump --temp-file template3.py --disass-file spike-demo.s --modif-disass-path ./modif_disass/ --stat seq_extr
```
